// pass by reference : array, object
var name = "Alice";
var age = 2;
var gender = "femail";

var sv1 = {
  name: "Alice",
  age: 2,
  gender: "femail",
};

var sv2 = {
  name: "Bob",
  age: 2,
  gender: "male",
};

console.log("sv1 before ", sv1);
sv1.age = 5;
console.log("sv1 after ", sv1);

var cat1 = {
  name: "Black ",
  score: 10,
  talk: function () {
    console.log("gâu gâu, i am ", this.name);
  },
};
cat1.talk();
// dynamic key
var key = "age";
cat1[key] = " Black Adam";

console.log("cat1: ", cat1);

const cat2 = cat1;

console.log("cat1: ", cat1);
console.log("cat2: ", cat2);
cat2.score = 1;
cat2.isLogin = true;
const arr = [];
arr.push(1);
console.log("arr: ", arr);
console.log("after");
console.log("cat1: ", cat1);
console.log("cat2: ", cat2);

var a = 5;
var b = a;
b = 10;
console.log(a);

/**
 * object ={ key : value }
 */
