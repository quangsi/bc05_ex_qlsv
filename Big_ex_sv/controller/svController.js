function layThongTinTuForm() {
  // lấy dữ liệ từ form

  var maSv = document.getElementById("txtMaSV").value;
  var tenSv = document.getElementById("txtTenSV").value;
  var email = document.getElementById("txtEmail").value;
  var matKhau = document.getElementById("txtPass").value;
  var diemToan = document.getElementById("txtDiemToan").value;
  var diemLy = document.getElementById("txtDiemLy").value;
  var diemHoa = document.getElementById("txtDiemHoa").value;
  // tạo sv
  var sv = new SinhVien(maSv, tenSv, email, matKhau, diemToan, diemLy, diemHoa);
  return sv;
}

function renderDssv(svArr) {
  var contentHTML = "";
  for (var index = 0; index < svArr.length; index++) {
    var currentSv = svArr[index];

    var contentTr = `
    
       <td>${currentSv.ma}</td>
       <td>${currentSv.ten}</td>
       <td>${currentSv.email}</td>
       <td>${currentSv.tinhDTB().toFixed(1)}</td>
       <td>
       <button onclick="xoaSv('${
         currentSv.ma
       }')" class="btn btn-danger">Xoá</button>
       <button class="btn btn-warning"
       
       onclick="suaSv('${currentSv.ma}')"
       >Sửa</button>
       </td>
    </tr>
    `;
    contentHTML += contentTr;
  }
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

function timKiemViTri(id, svArr) {
  for (var index = 0; index < svArr.length; index++) {
    var item = svArr[index];
    // nếu tìm thấy thì dừng function và trả về index hiện tại
    if (item.ma == id) {
      return index;
    }
  }
  // quy định: nếu ko tìm thấy thì trả về -1
  return -1;
}
function showThongTinLenForm(sv) {
  document.getElementById("txtMaSV").value = sv.ma;
  document.getElementById("txtTenSV").value = sv.ten;
  document.getElementById("txtEmail").value = sv.email;
  document.getElementById("txtPass").value = sv.matKhau;
  document.getElementById("txtDiemToan").value = sv.toan;
  document.getElementById("txtDiemLy").value = sv.ly;
  document.getElementById("txtDiemHoa").value = sv.hoa;
}

function resetForm() {
  document.getElementById("formQLSV").reset();
}

function showMessageErr(idErr, message) {
  document.getElementById(idErr).innerHTML = message;
}
